<h1 align=center># Digital Republic Code Challenge</h1>


## 👨🏻‍💻 Sobre o Projeto

Uma aplicação web que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala. Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede. Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar.

---

## 👨🏻‍💻 Tecnologias

- React
- Typescript
- Hooks
- Styled-components

---

## 💻 Instalação e uso

```
$ git clone https://github.com/jaquelinepires/DR-CodeChallenge.git
```

 Vá para a pasta DR-CodeChallenge-main:

```
$ cd DR-CodeChallenge-main
```

 Instale as dependêcias:

```
$ yarn
```

Agora, execute o aplicativo:

```
$ yarn start  
```
Caso não abra automaticamente, acesse http://localhost:3000 para visualizá-lo no navegador.

___
Feito com 💙 by Jaqueline Pires