import styled from "styled-components";

export const Container = styled.div`
  margin-left: 1rem;
  margin-right: 1rem;

  h2 {
    padding: 1rem 0 0 2rem;
    color: #2d3748;
    margin-bottom: 1.5rem;
  }
  input[id="Height"] {
    margin-left: -6px;
  }
  input[id="door"] {
    margin-left: 13px;
  }
  input[id="windows"] {
    margin-left: 13px;
  }
`;
export const ContainerInput = styled.div``;

export const Alert = styled.div`
  margin: 10px 0 0 25px;
  max-width: 200px;
  font-size: 14px;
  color: red;
`;
