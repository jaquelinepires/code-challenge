import { useState } from "react";
import { Input } from "../Input";
import {
  getAreaDoors,
  getAreaWall,
  getAreaWindow,
  DimensionWall,
  DoorOnWall,
} from "../../utils/CalcPaintCans";
import { Container, ContainerInput, Alert } from "./styles";

interface Props {
  name: string;
  areaToPaint: (value: number) => void;
}

export function Wall({ name, areaToPaint }: Props) {
  const DOORS_AND_WINDOWS_PERCENTAGE_OF_WALL_AREA = 50;

  const [height, setHeight] = useState(0);
  const [width, setWidth] = useState(0);
  const [doors, setDoors] = useState(0);
  const [window, setWindow] = useState(0);

  const [heightError, setHeightError] = useState(false);
  const [widthError, setWidthError] = useState(false);
  const [doorsError, setDoorsError] = useState(false);

  const areaWall = getAreaWall(width, height);
  const areaWindows = getAreaWindow(window);
  const areaDoors = getAreaDoors(doors);

  const handleHeightWall = (heightWall: number) => {
    if (DimensionWall(heightWall)) {
      setHeightError(true);
      setHeight(0);
    } else {
      setHeightError(false);
      setHeight(heightWall);
    }
  };
  const handleWidthWall = (widthWall: number) => {
    if (DimensionWall(widthWall)) {
      setWidthError(true);
      setWidth(0);
    } else {
      setWidthError(false);
      setWidth(widthWall);
    }
  };

  const handleDoorsWall = (quantityDoors: number) => {
    if (DoorOnWall(height) || quantityDoors === 0) {
      setDoorsError(false);
      setDoors(quantityDoors);
    } else {
      setDoorsError(true);
      setDoors(0);
    }
  };

  const isAvailableWall = () => {
    const areaDoorsAndWindows = parseFloat(areaDoors) + parseFloat(areaWindows);
    const percentage = (areaDoorsAndWindows / parseFloat(areaWall)) * 100;

    if (percentage > DOORS_AND_WINDOWS_PERCENTAGE_OF_WALL_AREA) {
      areaToPaint(0);
      return false;
    }
    const areaFinal = parseFloat(areaWall) - areaDoorsAndWindows;
    areaToPaint(areaFinal);
    return true;
  };

  return (
    <Container>
      <h2>{name}</h2>

      <ContainerInput>
        <Input
          label="Altura parede :"
          id="Height"
          type="number"
          name="Height"
          required
          onChange={(e) => {
            handleHeightWall(e.target.valueAsNumber);
          }}
        />
        {heightError && <Alert>Altura deve ter entre 1m a 15m</Alert>}
        <Input
          label="Largura parede: "
          name="width"
          type="number"
          id="width"
          required
          onChange={(e) => {
            handleWidthWall(e.target.valueAsNumber);
          }}
        />
        {widthError && <Alert>Largura deve ter entre 1m a 15m</Alert>}

        <Input
          label="Qtd Janela: "
          name="windows"
          id="windows"
          type="number"
          required
          onChange={(e) => {
            setWindow(e.target.valueAsNumber);
          }}
        />

        <Input
          label="Qtd Portas: "
          name="door"
          id="door"
          type="number"
          required
          onChange={(e) => {
            handleDoorsWall(e.target.valueAsNumber);
          }}
        />
        {doorsError && (
          <Alert>
            A porta não respeita o tamanho mínimo da altura da parede de 2,2m
          </Alert>
        )}

        {!isAvailableWall() && (
          <Alert>
            O total de área das portas e janelas deve ser no máximo 50% da área
            de parede.
          </Alert>
        )}
      </ContainerInput>
    </Container>
  );
}
