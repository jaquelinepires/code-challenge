import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  gap: 1.56rem;

  input {
    background: #fff;
    border: 1px solid #cbd5e0;
    outline: 0;
    padding: 0 1rem;
    border-radius: 4px;
    height: 1.87rem;
    width: 3rem;
    margin-top: 1rem;
    margin-left: -1rem;

    &[type="number"]::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }
    &[type="number"] {
      -moz-appearance: textfield;
      appearance: textfield;
    }
  }
`;
