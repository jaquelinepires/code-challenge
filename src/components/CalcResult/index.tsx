import { Container } from "./styles";

interface ResultProps {
  calculate: {
    size: string;
    amount: number;
    counter: number;
  }[];
}

export function CalcResult({ calculate }: ResultProps) {
  return (
    <Container>
      <span>Necessita adquirir uma das opções abaixo:</span>
      <ul>
        {calculate.map((item) => (
          <li key={item.size}>
            Quantidade necessária: {item.counter} Lata(s)
            <br /> tamanho da latas: {item.size}
          </li>
        ))}
      </ul>
    </Container>
  );
}
