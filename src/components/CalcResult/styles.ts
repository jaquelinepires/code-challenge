import styled from "styled-components";

export const Container = styled.div`
  margin-top: 1rem;
  margin-bottom: 1rem;
  padding: 1rem;
  background: #e2e8f0;
  color: #4a5568;
  border: 1px solid #e2e8f1;
  border-radius: 0.9rem;

  ul {
    margin-top: 10px;
    li {
      list-style-type: none;
      margin-left: 20px;
      margin-top: 10px;
    }
  }
`;
