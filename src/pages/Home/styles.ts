import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #5b5ac5;
  height: 200px;
`;
export const Title = styled.h1`
  color: #ffff;
  padding-block: 1rem;
  display: flex;
  align-items: center;
  margin-bottom: 3rem;
  margin-top: 1rem;
`;

export const Form = styled.form`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 0.9rem;
  border: 1px solid #ffff;
  box-shadow: -2px 2px 2px -2px #cbd5e0;
`;
export const ContainerWalls = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: auto;
  gap: 4rem;

  h4 {
    color: #fff;
  }
`;

export const Button = styled.button`
  color: #fff;
  background: #6564db;
  font-weight: bold;
  padding: 0.8rem 4rem;
  border: none;
  border-radius: 0.9rem;
  transition: filter 0.2s;
  margin-top: 2rem;
  margin-bottom: 1rem;

  display: flex;
  align-items: center;
  justify-content: center;
  letter-spacing: 0.1rem;

  &:hover {
    filter: brightness(0.9);
  }
`;
