import { FormEvent, useState } from "react";
import { FcCalculator } from "react-icons/fc";
import { Wall } from "../../components/Wall";
import { CalcResult } from "../../components/CalcResult";
import { Container, Title, Form, ContainerWalls, Button } from "./styles";

interface CAN_TYPES {
  size: string;
  amount: number;
  counter: number;
}

export function Home() {
  const CAN_TYPES = [
    { size: "0.5L", amount: 0.5, counter: 0 },
    { size: "2.5L", amount: 2.5, counter: 0 },
    { size: "3.6L", amount: 3.6, counter: 0 },
    { size: "18L", amount: 18, counter: 0 },
  ];

  const [areaWall1, setAreaWall1] = useState(0);
  const [areaWall2, setAreaWall2] = useState(0);
  const [areaWall3, setAreaWall3] = useState(0);
  const [areaWall4, setAreaWall4] = useState(0);
  const [sizeGetWall, setSizeGetWall] = useState<CAN_TYPES[]>();

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();

    const totalAreaWall = areaWall1 + areaWall2 + areaWall3 + areaWall4;
    const totalLitroPaint = totalAreaWall / 5;

    const sizeCans = CAN_TYPES.map((item: CAN_TYPES) => {
      return {
        ...item,
        counter: Math.ceil(totalLitroPaint / item.amount),
      };
    });

    setSizeGetWall(sizeCans);
  };

  return (
    <Container>
      <Title>
        <FcCalculator />
        Wall Paint
      </Title>

      <Form onSubmit={handleSubmit}>
        <ContainerWalls>
          <Wall name="Wall #1" areaToPaint={setAreaWall1} />
          <Wall name="Wall #2" areaToPaint={setAreaWall2} />
          <Wall name="Wall #3" areaToPaint={setAreaWall3} />
          <Wall name="Wall #4" areaToPaint={setAreaWall4} />
        </ContainerWalls>

        <Button type="submit">Calculate</Button>

        {!!sizeGetWall && <CalcResult calculate={sizeGetWall} />}
      </Form>
    </Container>
  );
}
