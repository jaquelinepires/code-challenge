const WINDOW_AREA = 2.0 * 1.2;
const DOOR_WIDTH = 0.8;
const DOOR_HEIGHT = 1.9; 

export const getAreaWall = (height: number, width: number) => {
  const area = width * height;
  return area.toFixed(2);
};
export const getAreaWindow = (window: number) => {
  const area = window * WINDOW_AREA;
  return area.toFixed(2);
};

export const getAreaDoors = (doors: number) => {
  const area = doors * DOOR_WIDTH * DOOR_HEIGHT;
  return area.toFixed(2);
};

export const DimensionWall = (dimensionWall: number) => {
  if ( dimensionWall < 1 || dimensionWall > 15) { 
    return true; 
  }
  return false;
};
export const DoorOnWall = (height: number) => {
  if (height >= DOOR_HEIGHT + 0.3) {
    return true;
  }
  return false;
};


